
// const npsUtils = require('nps-utils')

const nodeArgs = 'src/bootstrap.ts'

module.exports = {
  scripts: {
    default: {
      script: `node -r ts-node/register ${nodeArgs}`,
      description: 'Starts the graphql server.',
    },
    dev: {
      script: `nodemon`,
      description: 'Runs the graphql app in watch mode with attachable debugger.',
      server: {
        script: `node --inspect=9222 -r ts-node/register ${nodeArgs}`,
        description: 'Runs the graphql app with attachable debugger.',
        hiddenFromHelp: true,
      }
    },
    npm: {
      watch: {
        script: `nodemon --config nodemon.npm.json`,
        description: 'Automatically installs dependencies whenever package-lock.json updates.',
      },
    },
    docker: {
      script: `nps npm.watch`,
    },
    test: {
      script: `jest --config ./jest.js --runInBand`,
      description: 'Runs the unit tests.',
    },
    lint: {
      script: `tslint --project .`,
      description: 'Lints the code.',
    },
    ts: {
      script: `tsc --noEmit --project .`,
      description: 'Type-checks the code.',
    },
    check: {
      script: 'nps lint test ts',
      description: 'Runs the checks to validate the application (test & lint).',
    },
    generate: {
      module: {
        script: `yo graphql:module`,
        description: 'Generates a new module in the graphql application.',
      },
    },
  },
}
