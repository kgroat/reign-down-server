
import 'reflect-metadata'
import { createDb } from './db'

if (!(Symbol as any).asyncIterator) {
  (Symbol as any).asyncIterator = Symbol.for('Symbol.asyncIterator')
}

async function bootstrap () {
  await createDb()
  require('./server')
}

bootstrap().catch((err) => {
  console.error('Something went wrong during app bootstrap:')
  console.error(err)
  process.exit(1)
})
