
import { randomBytes } from 'crypto'

function randomString () {
  return randomBytes(3).toString()
}

export class PackageNotFoundError extends Error {}
export class NpmUnreachableError extends Error {}

export const getPackageData = jest.fn((name: string) => Promise.resolve({
  _id: name,
  rev: randomString(),
  'dist-tags': { 'latest': '1.0.0' },
  name,
  time: { '1.0.0': '2016-05-17T18:43:22.882Z' },
  versions: {
    '1.0.0': {
      _id: randomString(),
      _nodeVersion: '1.0.0',
      _npmOperationalInternal: {
        host: 's3://npm-registry-packages',
        tmp: randomString(),
      },
      _npmUser: {
        name: 'Test Dude',
        email: 'test@dude.com',
      },
      _npmVersion: '1.0.0',
      _resolved: randomString(),
      _shasum: randomString(),
      dist: {
        shasum: randomString(),
        tarball: `https://registry.npmjs.org/${name}/-/${name}-1.0.0.tgz`,
      },
      name,
      version: '1.0.0',
    },
  },
}))
