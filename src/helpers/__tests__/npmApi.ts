
import origFetch from 'node-fetch'
import { MockFetch } from '../../../mocks'

import { getPackageData, PackageNotFoundError, NpmUnreachableError } from '../npmApi'

const fetch = origFetch as MockFetch

describe('npmApi', () => {
  describe(getPackageData.name, () => {
    it('should return the value of the json response if successful', async () => {
      const returnedData = {
        some: 'data',
      }
      fetch.mockJsonResponseOnce(returnedData)

      const result = await getPackageData('test-package')

      expect(result).toBe(returnedData)
    })

    it('should throw a PackageNotFoundError if the response status is 404', async () => {
      fetch.mockResponseWithStatus(404)
      try {
        await getPackageData('test-package')
        fail('should have thrown an error')
      } catch (err) {
        expect(err).toBeInstanceOf(PackageNotFoundError)
      }
    })

    it('should throw a NpmUnreachableError if the response status is any error status besides 404', async () => {
      fetch.mockResponseWithStatus(500)
      try {
        await getPackageData('test-package')
        fail('should have thrown an error')
      } catch (err) {
        expect(err).toBeInstanceOf(NpmUnreachableError)
      }
    })
  })
})
