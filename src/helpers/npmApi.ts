
import fetch from 'node-fetch'
import { NotFoundError, InternalServerError } from '../errors'

export interface Author {
  name?: string
  email?: string
}

export interface RepositoryData {
  type?: string // Usually 'git'
  url: string
  directory?: string // Used for monorepos
}

export interface DistributionData {
  fileCount?: number
  integrity?: string
  'npm-signature'?: string // a PGP signature
  shasum: string
  tarball: string
  unpackedSize?: number
}

export interface PackageVersionData {
  _from?: string
  _hasShrinkwrap?: boolean
  _id: string
  _nodeVersion: string
  _npmOperationalInternal: { host: string, tmp: string }
  _npmUser: Required<Author>
  _npmVersion: string
  _resolved: string
  _shasum: string
  author?: Author
  authors?: Author[]
  dependencies?: Record<string, string>
  description?: string
  directories?: Record<string, string>
  dist: DistributionData
  license?: string
  main?: string
  maintainers?: Author[]
  name: string
  repository?: RepositoryData
  scripts?: Record<string, string>
  version: string

  [key: string]: any // for any non-standard package.json properties
}

export interface PackageData {
  _id: string // same as name
  _rev: string
  bugs?: { url?: string }
  contributors?: Author[]
  description?: string
  'dist-tags': Record<string, string>
  homepage?: string
  keywords?: string[]
  license?: string
  maintainers?: Author[]
  name: string
  readme?: string // This is usually in markdown
  readmeFilename?: string
  repository?: RepositoryData
  time: Record<string, string>
  users?: Record<string, boolean>
  versions: Record<string, PackageVersionData>
}

export class PackageNotFoundError extends NotFoundError<PackageData> {
  constructor (name: string) {
    super('Package', { name })
  }
}

export class NpmUnreachableError extends InternalServerError {
  constructor (statusCode: number, statusText: string) {
    super('Cannot reach npm', 'NPM_UNREACHABLE', { statusCode, statusText })
  }
}

export async function getPackageData (name: string): Promise<PackageData> {
  const response = await fetch(`https://registry.npmjs.org/${name}`)
  if (response.status === 404) {
    throw new PackageNotFoundError(name)
  } else if (response.status !== 200) {
    throw new NpmUnreachableError(response.status, response.statusText)
  }
  return response.json()
}
