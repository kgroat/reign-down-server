
import { UserInputError, ApolloError } from 'apollo-server'
import { FilterQuery } from 'mongodb'

import { IS_PROD } from './config'

export class InternalServerError extends ApolloError {
  readonly name = Object.getPrototypeOf(this).constructor.name
  constructor (message: string, code: string, extraProps?: any) {
    super(message, code, !IS_PROD ? extraProps : undefined)
  }
}

const NOT_FOUND_CODE = 'NOT_FOUND'
export class NotFoundError<T> extends InternalServerError {
  constructor (
    public modelName: string,
    query: FilterQuery<T>,
    message = `${modelName} not found`,
  ) {
    super(message, NOT_FOUND_CODE, query)
  }
}

export class InputValidationError extends UserInputError {
  constructor (message: string) {
    super(message)
  }
}
