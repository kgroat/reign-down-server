
import { ObjectID } from 'mongodb'
import { IResolvers } from 'apollo-server'
import { authorize } from '../../helpers/authorize'
import gql from '../../helpers/noopTag'
import { Context } from '../../context'
import { User } from '../user/user.types'
import { UserService } from '../user/user.service'
import { PaginationRequest } from '../common/common.types'

import { PostService } from './post.service'
import { PostPubSub } from './post.pubsub'
import { Post, CreatePostRequest, UpdatePostRequest } from './post.types'

export const typeDefs = gql`
  extend type Query {
    """
    Used to retrieve all \`Post\`s.
    Supports pagination.
    """
    posts (pagination: PaginationData): [Post!]!

    """
    Used to retrieve a particular \`Post\` by its \`_id\`.
    """
    post (postId: ID!): Post!
  }

  extend type Mutation {
    """
    Create a new post.
    [AUTH: required]
    """
    createPost (content: String!): Post!

    """
    Modify one of your own posts.
    [AUTH: owner]
    """
    updatePost (postId: ID!, data: UpdatePost!): Post!
  }

  extend type Subscription {
    """
    Get a \`Post\` by its \`_id\`, and listen for updates.
    """
    post (postId: ID!): Post!
  }

  input UpdatePost {
    content: String
  }

  type Post implements Document {
    _id: ID!
    content: String!

    """
    The \`User\` who created the post.
    """
    poster: User!
  }
`

export const postResolvers = {
  Query: {
    async posts (_source: any, { pagination }: PaginationRequest): Promise<Post[]> {
      const posts = await PostService.instance.findAll(pagination)
      return posts
    },
    async post (_source: any, { postId }: { postId: ObjectID }): Promise<Post | null> {
      return PostService.instance.findById(postId)
    },
  },
  Mutation: {
    async createPost (_source: any, args: CreatePostRequest, ctx: Context): Promise<Post> {
      const user = await authorize(ctx)
      return PostService.instance.createPost(user._id!, args)
    },
    async updatePost (_source: any, args: UpdatePostRequest, ctx: Context): Promise<Post> {
      const user = await authorize(ctx)
      const post = await PostService.instance.updatePost(user._id!, args)
      PostPubSub.instance.publishUpdate(post)
      return post
    },
  },
  Subscription: {
    post: {
      async subscribe (_source: any, { postId }: { postId: ObjectID }): Promise<AsyncIterator<Post>> {
        const post = await PostService.instance.findById$(postId)
        return PostPubSub.instance.listenForUpdates(post)
      },
    },
  },
  Post: {
    async poster (source: Post): Promise<User> {
      return UserService.instance.findById$(source.posterId)
    },
  },
}

export const resolvers: IResolvers<Post, Context> = postResolvers
