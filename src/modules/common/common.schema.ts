
import { GraphQLScalarType } from 'graphql'
import { Kind } from 'graphql/language'
import { ObjectID } from 'mongodb'
import { IResolvers } from 'apollo-server'
import gql from '../../helpers/noopTag'
import { Context } from '../../context'

import { Document } from './common.types'

export const typeDefs = gql`
  """
  The input type used to request a range of data from an endpoint which supports pagination.
  """
  input PaginationData {
    """
    The number of entries to skip before taking any.
    [DEFAULT: 0]
    """
    skip: Int

    """
    The maximum number of entries to return.
    [DEFAULT: 20]
    [MAXIMUM: 50]
    """
    limit: Int
  }

  scalar ID

  interface Document {
    _id: ID!
  }
`

export const resolvers: IResolvers<Document, Context> = {
  ID: new GraphQLScalarType({
    name: 'ID',
    serialize (_id): string {
      if (_id instanceof ObjectID) {
        return _id.toHexString()
      } else if (typeof _id === 'string') {
        return _id
      } else {
        throw new Error(`${Object.getPrototypeOf(_id).constructor.name} not convertible to ID.`)
      }
    },
    parseValue (_id): ObjectID {
      if (typeof _id === 'string') {
        return ObjectID.createFromHexString(_id)
      } else {
        throw new Error(`${typeof _id} not convertible to ID`)
      }
    },
    parseLiteral (ast): ObjectID {
      if (ast.kind === Kind.STRING) {
        return ObjectID.createFromHexString(ast.value)
      } else {
        throw new Error(`${ast.kind} not convertible to ID`)
      }
    },
  }),
}
