
import { IResolvers } from 'apollo-server'
import * as glob from 'glob'
import { join } from 'path'
import gql from '../helpers/noopTag'

import { HeartbeatPubSub } from './common/common.pubsub'

const baseTypeDef = gql`
type Query {
  """
  An endpoint which can be used to check whether or not the server is working.
  """
  healthCheck: Boolean!
}
type Subscription {
  """
  An endpoint which can be used to check whether or not the server is working.
  """
  healthCheck: Boolean!
}
type Mutation {
  """
  An endpoint which can be used to check whether or not the server is working.
  """
  healthCheck: Boolean!
}
`

interface SchemaFileContent {
  typeDefs: string
  resolvers?: IResolvers
}

const schemaFiles = glob.sync(join(__dirname, './**/!(__tests__|__mocks__)/*.schema.ts'))

let gatheredTypeDefs: string[] = [baseTypeDef]
let gatheredResolvers: IResolvers = {
  Query: {
    healthCheck: () => true,
  },
  Subscription: {
    healthCheck: () => HeartbeatPubSub.instance.listen(),
  },
  Mutation: {
    healthCheck: () => true,
  },
}

schemaFiles.forEach(file => {
  const { typeDefs, resolvers }: SchemaFileContent = require(file)

  gatheredTypeDefs.push(typeDefs)
  if (resolvers) {
    gatheredResolvers = {
      ...gatheredResolvers,
      ...resolvers,
      Query: {
        ...gatheredResolvers.Query,
        ...(resolvers.Query || {}),
      } as any,
      Mutation: {
        ...gatheredResolvers.Mutation,
        ...(resolvers.Mutation || {}),
      } as any,
      Subscription: {
        ...gatheredResolvers.Subscription,
        ...(resolvers.Subscription || {}),
      } as any,
    }
  }
})

export const typeDefs = gatheredTypeDefs
export const resolvers = gatheredResolvers
