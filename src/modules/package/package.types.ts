
import { Document } from '../common/common.types'
import { RepositoryData } from '../../helpers/npmApi'

export enum PackageSource {
  npm = 'npm',
}

export enum RepoSource {
  Other = 'oth',
  GitHub = 'gh',
  GitLab = 'gl',
}

export interface Package extends Document {
  name: string
  source: PackageSource
  repo?: PackageRepositoryInfo
}

export interface PackageRepositoryInfo {
  url: string
  source: RepoSource
}

export interface CreatePackageRequest {
  name: string
  repo?: RepositoryData
}
