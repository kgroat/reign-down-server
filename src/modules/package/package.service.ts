
import { getCollection } from '../../db'
import { DbService } from '../common/common.service'
import { PaginationData } from '../common/common.types'
import { getPackageData } from '../../helpers/npmApi'

import { Package, CreatePackageRequest, PackageRepositoryInfo, RepoSource, PackageSource } from './package.types'

const typePrefixRgx = /^\w+\+(.*)$/
const sshPrefixRgx = /^(?:ssh:\/?\/?)?(?:\w|-|\.)+@(.*)$/

const githubRgx = /^(?:git\+)?(?:https?:\/\/|ssh:\/?\/?)(?:git@)?github\.com\//i
const gitlabRgx = /^(?:git\+)?(?:https?:\/\/|ssh:\/?\/?)(?:git@)?gitlab\.com\//i

export class PackageService extends DbService<Package> {
  constructor (
    collection = getCollection<Package>('packages'),
  ) {
    super(collection)
  }

  static instance = new PackageService()

  async findAll (pagination?: PaginationData): Promise<Package[]> {
    const packages = await this.paginate(
      this.collection.find(),
      pagination,
    )
    return packages
  }

  async findByName (name: string): Promise<Package | null> {
    const somePackage = await this.collection.findOne({ name })
    return somePackage
  }

  async findOrTryInsert (name: string): Promise<Package> {
    const somePackage = await this.findByName(name)
    if (somePackage !== null) {
      return somePackage
    }

    const packageData = await getPackageData(name)

    return this.createPackage({
      name,
      repo: packageData.repository,
    })
  }

  async createPackage ({ name, repo: originalRepo }: CreatePackageRequest): Promise<Package> {
    let repo: PackageRepositoryInfo | undefined

    if (originalRepo) {
      repo = {
        url: this._cleanRepoUrl(originalRepo.url),
        source: this._getRepoSource(originalRepo.url),
      }
    }

    const somePackage: Package = {
      name,
      source: PackageSource.npm, // Always npm for now
      repo,
    }

    const result = await this.collection.insertOne(somePackage)
    if (result.insertedCount < 1) {
      throw new Error(`Package was not created correctly!`)
    }

    return {
      ...somePackage,
      _id: result.insertedId,
    }
  }

  private _cleanRepoUrl (url: string): string {
    if (!url) {
      return url
    }

    let match = typePrefixRgx.exec(url)
    if (match) {
      url = match[1]
    }

    match = sshPrefixRgx.exec(url)
    if (match) {
      url = match[1]
    }

    if (url.indexOf('http') !== 0) { // this also works if protocol is https
      url = `http://${url}` // can't assume https will work for any given repo
    }

    return url
  }

  private _getRepoSource (url: string): RepoSource {
    if (githubRgx.test(url)) {
      return RepoSource.GitHub
    } else if (gitlabRgx.test(url)) {
      return RepoSource.GitLab
    } else {
      return RepoSource.Other
    }
  }
}
