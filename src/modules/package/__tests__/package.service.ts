
import { fail } from 'assert'
import { Collection, InsertOneWriteOpResult, Cursor, ObjectID } from 'mongodb'
import { PackageService } from '../package.service'
import { Package, RepoSource } from '../package.types'
import { getPackageData as origGetPackageData, PackageData } from '../../../helpers/npmApi'

jest.mock('../../../helpers/npmApi')
jest.mock('../../../db')
jest.mock('../../common/common.service')
jest.mock('../../../config')

const getPackageData = (origGetPackageData) as jest.Mock<Promise<PackageData>, [string]>
const paginate: jest.Mock = (PackageService.instance as any).paginate
const collection: Collection = (PackageService.instance as any).collection

describe('PackageService', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('findAll', () => {
    // tslint:disable-next-line
    const find = collection.find as jest.Mock<Cursor<Package>, [Partial<Package>?]>

    it('should call this.collection.find once', async () => {
      await PackageService.instance.findAll()

      expect(find).toHaveBeenCalledTimes(1)
    })

    it('should call this.paginate once with the supplied pagination data', async () => {
      const paginationData = {}

      await PackageService.instance.findAll(paginationData)

      expect(paginate).toHaveBeenCalledTimes(1)
      expect(paginate.mock.calls[0][1]).toBe(paginationData)
    })
  })

  describe('findByName', () => {
    const findOne = collection.findOne as jest.Mock<Promise<Partial<Package> | null>, [Partial<Package>]>

    it('should call this.collection.findOne once with supplied name', async () => {
      const name = 'test-package'

      await PackageService.instance.findByName(name)

      expect(findOne).toHaveBeenCalledTimes(1)
      expect(findOne.mock.calls[0][0].name).toBe(name)
    })

    it('should not throw an error if no package is found', async () => {
      findOne.mockResolvedValueOnce(null)

      const findPromise = PackageService.instance.findByName('test-package')

      await expect(findPromise).resolves.not.toThrow()
    })
  })

  describe('findOrTryInsert', () => {
    const findOne = collection.findOne as jest.Mock<Promise<Partial<Package> | null>, [Partial<Package>]>
    const insertOne = collection.insertOne as jest.Mock<Promise<Partial<InsertOneWriteOpResult>>, [Package]>

    it('should call this.collection.findOne once with supplied name', async () => {
      const name = 'test-package'
      findOne.mockResolvedValueOnce({ name })

      await PackageService.instance.findOrTryInsert(name)

      expect(findOne).toHaveBeenCalledTimes(1)
      expect(findOne.mock.calls[0][0].name).toBe(name)
    })

    it('should call this.collection.insertOn if the package isn\'t in our DB but is on NPM', async () => {
      const name = 'test-package'
      findOne.mockResolvedValueOnce(null)
      getPackageData.mockResolvedValueOnce({ name } as PackageData)
      insertOne.mockResolvedValueOnce({ insertedCount: 1, insertedId: new ObjectID() })

      await PackageService.instance.findOrTryInsert(name)
      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].name).toBe(name)
    })

    it('should throw an error if getPackageData throws', async () => {
      const testError = {
        some: 'error',
      }
      findOne.mockResolvedValueOnce(null)
      getPackageData.mockRejectedValueOnce(testError)

      try {
        await PackageService.instance.findOrTryInsert('test-package')
        fail('should have thrown an error')
      } catch (err) {
        expect(err).toBe(testError)
      }
    })
  })

  describe('createPackage', () => {
    const insertOne = collection.insertOne as jest.Mock<Promise<Partial<InsertOneWriteOpResult>>, [Package]>

    it('should call this.collection.insertOne with the supplied name', async () => {
      const name = 'test-package'

      await PackageService.instance.createPackage({ name })

      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].name).toBe(name)
    })

    it('should call this.collection.insertOne with the corrected repo url', async () => {
      const githubBase = 'github.com/some-user/some-package.git'
      const repoUrl = `git+ssh:git@${githubBase}` // add in the stuff that npm would

      await PackageService.instance.createPackage({
        name: 'test-package',
        repo: {
          url: repoUrl,
        },
      })

      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].repo!.url).toBe(`http://${githubBase}`)
    })

    it('should call this.collection.insertOne with the repo source = GitHub', async () => {
      const githubBase = 'github.com/some-user/some-package.git'
      const repoUrl = `git+ssh:git@${githubBase}` // add in the stuff that npm would

      await PackageService.instance.createPackage({
        name: 'test-package',
        repo: {
          url: repoUrl,
        },
      })

      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].repo!.source).toBe(RepoSource.GitHub)
    })

    it('should call this.collection.insertOne with the repo source = GitLab', async () => {
      const gitlabBase = 'gitlab.com/some-user/some-package.git'
      const repoUrl = `git+ssh:git@${gitlabBase}` // add in the stuff that npm would

      await PackageService.instance.createPackage({
        name: 'test-package',
        repo: {
          url: repoUrl,
        },
      })

      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].repo!.source).toBe(RepoSource.GitLab)
    })

    it('should call this.collection.insertOne with the repo source = Other', async () => {
      const gitlabBase = 'example.com/some-user/some-package.git'
      const repoUrl = `git+ssh:git@${gitlabBase}` // add in the stuff that npm would

      await PackageService.instance.createPackage({
        name: 'test-package',
        repo: {
          url: repoUrl,
        },
      })

      expect(insertOne).toHaveBeenCalledTimes(1)
      expect(insertOne.mock.calls[0][0].repo!.source).toBe(RepoSource.Other)
    })

    it('should return a package whose _id is the inserted id', async () => {
      const insertedId = new ObjectID()
      insertOne.mockResolvedValueOnce({
        insertedId,
        insertedCount: 1,
      })

      const newPackage = await PackageService.instance.createPackage({
        name: 'test-package',
      })

      expect(newPackage._id).toBe(insertedId)
    })

    it('should throw an error if this.collection.insertOne does not succeed', async () => {
      insertOne.mockResolvedValueOnce({
        insertedCount: 0,
      })

      try {
        await PackageService.instance.createPackage({
          name: 'test-package',
        })
        fail('should have thrown an error')
      } catch (err) {
        expect(err).toBeDefined()
      }
    })
  })
})
