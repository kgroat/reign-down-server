
import { PackageService } from '../package.service'

import { packageResolvers } from '../package.schema'

jest.mock('../../../helpers/authorize')
jest.mock('../../user/user.service')
jest.mock('../package.service')

describe('package schema resolvers', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('Query', () => {
    describe('packages', () => {
      it('should call PackageService:findAll once', async () => {
        const pagination = {
          skip: 5,
        }

        await packageResolvers.Query.packages({}, { pagination })

        expect(PackageService.instance.findAll).toHaveBeenCalledTimes(1)
        expect(PackageService.instance.findAll).toHaveBeenCalledWith(pagination)
      })

      it('should return the result of PackageService:findAll', async () => {
        const packages = [{
          _id: 'test-package',
        }]
        ;(PackageService.instance.findAll as jest.Mock).mockResolvedValue(packages)

        const result = await packageResolvers.Query.packages({}, {})

        expect(result).toBe(packages)
      })
    })

    describe('package', () => {
      it('should call PackageService:findOrTryInsert once', async () => {
        const name = 'test-package'

        await packageResolvers.Query.package({}, { name })

        expect(PackageService.instance.findOrTryInsert).toHaveBeenCalledTimes(1)
        expect(PackageService.instance.findOrTryInsert).toHaveBeenCalledWith(name)
      })

      it('should return the result of PackageService:findOrTryInsert', async () => {
        const testPackage = {
          _id: 'test-package',
        }
        ;(PackageService.instance.findOrTryInsert as jest.Mock).mockResolvedValue(testPackage)

        const result = await packageResolvers.Query.package({}, { name: 'test-package' })

        expect(result).toBe(testPackage)
      })
    })
  })
})
