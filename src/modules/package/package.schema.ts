
import { IResolvers } from 'apollo-server'
import gql from '../../helpers/noopTag'
import { Context } from '../../context'
import { PaginationRequest } from '../common/common.types'

import { PackageService } from './package.service'
import { Package, PackageSource, RepoSource } from './package.types'

export const typeDefs = gql`
  extend type Query {
    """
    Used to retrieve all packages.
    Supports pagination.
    """
    packages (pagination: PaginationData): [Package!]!

    """
    Used to get a specific package based on its name.
    """
    package (name: String!): Package!
  }

  """
  The source registry of a given package.
  For now, it will always be \`NPM\`.
  """
  enum PackageSource {
    NPM
  }

  """
  The repository provider used for a given repo.
  This can be useful for gathering info such as stars and forks.
  """
  enum RepoSource {
    GITHUB
    GITLAB
    OTHER
  }

  """
  The source code repo location & information for a given package.
  """
  type Repository {
    url: String!
    source: RepoSource!
  }

  """
  A package which can be rated.
  """
  type Package {
    name: String!
    source: PackageSource
    repo: Repository
  }
`

export const packageResolvers = {
  Query: {
    async packages (_source: any, { pagination }: PaginationRequest): Promise<Package[]> {
      const packages = await PackageService.instance.findAll(pagination)
      return packages
    },
    async package (_source: any, { name }: { name: string }): Promise<Package | null> {
      return PackageService.instance.findOrTryInsert(name)
    },
  },
  PackageSource: {
    NPM: PackageSource.npm,
  },
  RepoSource: {
    GITHUB: RepoSource.GitHub,
    GITLAB: RepoSource.GitLab,
    OTHER: RepoSource.Other,
  },
}

export const resolvers: IResolvers<Package, Context> = packageResolvers
