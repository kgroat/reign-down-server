
import { ObjectID } from 'mongodb'
import { PaginationData } from '../../common/common.types'
import { Package, CreatePackageRequest, PackageSource } from '../package.types'

const DEFAULT_LIMIT = 20

const defaultPagination: PaginationData = {
  limit: DEFAULT_LIMIT,
}

export const PackageService = {
  instance: {
    findAll: jest.fn(async ({ limit = DEFAULT_LIMIT }: PaginationData = defaultPagination) => {
      return [...new Array(limit)].map<Package>(() => ({
        _id: new ObjectID(),
        name: new ObjectID().toHexString(),
        source: PackageSource.npm,
      }))
    }),
    findByName: jest.fn(async (name: string): Promise<Package | null> => {
      return {
        _id: new ObjectID(),
        name,
        source: PackageSource.npm,
      }
    }),
    findOrTryInsert: jest.fn(async (name: string): Promise<Package> => {
      return {
        _id: new ObjectID(),
        name,
        source: PackageSource.npm,
      }
    }),
    createPackage: jest.fn(async ({ name }: CreatePackageRequest): Promise<Package> => {
      return {
        _id: new ObjectID(),
        name,
        source: PackageSource.npm,
      }
    }),
  },
}
