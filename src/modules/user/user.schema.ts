
import { IResolvers } from 'apollo-server'
import { ObjectID } from 'mongodb'
import gql from '../../helpers/noopTag'
import { Context } from '../../context'
import { PostService } from '../post/post.service'
import { PaginationRequest } from '../common/common.types'
import { Post } from '../post/post.types'

import { UserService } from './user.service'
import { User, CreateUserRequest } from './user.types'

export const typeDefs = gql`
  extend type Query {
    """
    Used to retrieve a given \`User\` based on its \`username\`.
    """
    user (username: String!): User

    """
    Used to retrieve a given \`User\` based on its \`_id\`.
    """
    userById (userId: ID!): User

    """
    Used to retrieve all of the users.
    Supports pagination.
    """
    users (pagination: PaginationData): [User!]!
  }

  extend type Mutation {
    """
    Used to register a new user account.
    """
    createUser (
      username: String!,
      password: String!,
      verifyPassword: String!,
    ): User!
  }

  type User implements Document {
    _id: ID!
    username: String!

    """
    All of the posts created by a particular User.
    """
    posts (pagination: PaginationData): [Post!]!
  }
`

export const userResolvers = {
  Query: {
    async user (_source: any, { username }: { username: string }): Promise<User | null> {
      return UserService.instance.findByUsername(username)
    },
    async userById (_source: any, { userId }: { userId: ObjectID }): Promise<User | null> {
      return UserService.instance.findById(userId)
    },
    async users (_source: any, { pagination }: PaginationRequest): Promise<User[]> {
      return UserService.instance.findAll(pagination)
    },
  },
  Mutation: {
    async createUser (_source: any, args: CreateUserRequest): Promise<User> {
      return UserService.instance.createUser(args)
    },
  },
  User: {
    async posts ({ _id }: User, { pagination }: PaginationRequest): Promise<Post[]> {
      return PostService.instance.findAllByPoster(_id!, pagination)
    },
  },
}

export const resolvers: IResolvers<User, Context> = userResolvers
