
import * as fetchTypes from 'node-fetch'

type Fetch = typeof fetchTypes.default
export interface MockFetch extends Fetch, jest.Mock<Promise<fetchTypes.Response>, [fetchTypes.RequestInfo, fetchTypes.RequestInit?]> {
  mockJsonResponse (jsonObj: any): void
  mockJsonResponseOnce (jsonObj: any): void
  mockTextResponse (text: string): void
  mockTextResponseOnce (text: string): void
  mockResponseWithStatus (code: number, jsonObj?: any): void
}
