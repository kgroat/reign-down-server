
export class ApolloError extends Error {}

export class AuthenticationError extends Error {}

export class UserInputError extends Error {}
