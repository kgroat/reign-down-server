
import * as fetchTypes from 'node-fetch'
import { Readable } from 'stream'

import { MockFetch } from '../mocks'

function createEmptyResponse (req: fetchTypes.RequestInfo): fetchTypes.Response {
  const response: fetchTypes.Response = {
    body: new Readable(),
    bodyUsed: false,
    arrayBuffer: jest.fn(() => Promise.reject(new Error())),
    blob: jest.fn(() => Promise.reject(new Error())),
    json: jest.fn(() => Promise.reject(new Error())),
    text: jest.fn(() => Promise.reject(new Error())),
    textConverted: jest.fn(() => Promise.reject(new Error())),
    buffer: jest.fn(() => Promise.reject(new Error())),
    size: 0,
    timeout: 0,

    headers: {} as fetchTypes.Headers,
    ok: true,
    status: 200,
    statusText: 'OK',
    type: 'default' as fetchTypes.ResponseType,
    url: typeof req === 'string' ? req : req.url,
    clone: jest.fn(() => Object.assign(response)),
  }

  return response
}

function createJsonResponse (req: fetchTypes.RequestInfo, jsonObj: any): fetchTypes.Response {
  const response = createEmptyResponse(req)
  response.json = jest.fn(() => Promise.resolve(jsonObj))
  response.text = jest.fn(() => Promise.resolve(JSON.stringify(jsonObj)))

  return response
}

function createTextResponse (req: fetchTypes.RequestInfo, text: string): fetchTypes.Response {
  const response = createEmptyResponse(req)
  response.text = jest.fn(() => Promise.resolve(text))

  return response
}

const mockFetch = jest.fn((req: fetchTypes.RequestInfo, _init?: fetchTypes.RequestInit): Promise<fetchTypes.Response> => (
  Promise.resolve(createEmptyResponse(req))
)) as MockFetch

mockFetch.mockJsonResponse = (jsonObj: any) => {
  mockFetch.mockImplementation((req) => (
    Promise.resolve(createJsonResponse(req, jsonObj))
  ))
}

mockFetch.mockJsonResponseOnce = (jsonObj: any) => {
  mockFetch.mockImplementationOnce((req) => (
    Promise.resolve(createJsonResponse(req, jsonObj))
  ))
}

mockFetch.mockTextResponse = (text: string) => {
  mockFetch.mockImplementation((req) => (
    Promise.resolve(createTextResponse(req, text))
  ))
}

mockFetch.mockTextResponseOnce = (text: string) => {
  mockFetch.mockImplementationOnce((req) => (
    Promise.resolve(createTextResponse(req, text))
  ))
}

mockFetch.mockResponseWithStatus = (code: number, jsonObj?: any) => {
  mockFetch.mockImplementationOnce((req) => {
    const response = createJsonResponse(req, jsonObj)
    response.ok = code < 400
    response.status = code
    return Promise.resolve(response)
  })
}

export default mockFetch
