#!/usr/bin/env bash

if [ -z $* ]; then
  $(npm bin)/nps "test"
else
  $(npm bin)/nps "test --collectCoverage=false $*"
fi
